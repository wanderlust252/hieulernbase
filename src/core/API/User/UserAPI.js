import MyService from "../service";

const UserAPI = {
    getAllUser: async () => {

        var listUser = null;
        await MyService.getRequestData("/api/user")
            .then(data => listUser = data)
            .catch(err => console.log(err))
        return listUser;
    },
}
 

export default UserAPI;