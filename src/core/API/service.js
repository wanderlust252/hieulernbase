import { MyConfig } from "../config/Config";
// import MyUtil from "../util/MyUtil";
import {AsyncStorage} from 'react-native'


const getHeaders = (token) => {
    // const token = reactLocalStorage.get("user.token", "");
    if (!token) token = "";
    return {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': token
    }
}
// const getHeadersImage = (token) => {
//     // const token = reactLocalStorage.get("user.token", "");
//     if (!token) token = "";
//     return {
//         'Accept': 'application/json',
//         'Content-Type': 'multipart/form-data',
//         'Authorization': token
//     }
// }
const convertURLParameter = (path, params) => {
    var url = BASE_URL + path;
    if (params) {
        var esc = encodeURIComponent;
        var query = Object.keys(params)
            .map(k => esc(k) + '=' + esc(params[k]))
            .join('&');
        url += "?" + query;

    }
    return url;
}

const BASE_URL = MyConfig.getBaseUrl();

const MyService = {
    getRequestData: async function (path, params, token) {
        var url = convertURLParameter(path, params);
        var result = await fetch(url, {
            method: 'GET',
            headers: getHeaders(token),
            params: params
        })
            .then(response => response.json())
            .then(json => {
                return json;
            })
            .catch(e => {
                return { code: "error", message: e }
            });
        return result;//fetch
    }
    ,
    postRequestData: async function (url, data, token) {
        var result = await fetch(BASE_URL + url, {
            method: 'POST',
            headers: getHeaders(token),
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(json => {
                return json;
            })
            .catch(e => {
                return { code: "error", message: e }
            });
        return result;
    },
    // postRequestDataImage: async function (url, data, token) {
    //     var result = await fetch(BASE_URL + url, {
    //         method: 'POST',
    //         headers: getHeadersImage(token),
    //         body: JSON.stringify(data)
    //     })
    //         .then(response => response.json())
    //         .then(json => {
    //             return json;
    //         })
    //         .catch(e => {
    //             return { code: "error", message: e }
    //         });
    //     return result;
    // },
    putRequestData: async (url, data, token) => {
        var result = await fetch(BASE_URL + url, {
            method: 'PUT',
            headers: getHeaders(token),
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(json => {
                return json;
            })
            .catch(e => {
                return { code: "error", message: e }
            });
        return result;
    },
    // handleUploadPhoto: async (photo) => {
    //     var user = await AsyncStorage.getItem("userInfo");
    //     user = user? JSON.parse(user):""
    //     var result;
    //     token = user.user_acc_tokn
    //     await fetch(BASE_URL + "/users/upload-avatar", {
    //         method: "POST",
    //         body: MyUtil.createFormData(photo, "avatar"),
    //         headers: getHeadersImage(token)
    //     })
    //         .then(response => response.json())
    //         .then(response => {
    //             result = response;
    //         })
    //         .catch(error => {
    //             console.log("upload error", error);
    //             // alert("Upload failed!");
    //         });
    //     return result;
    // },

    handleUpProduce: async (data) => {
        var user = await AsyncStorage.getItem("userInfo");
        user = user? JSON.parse(user):""
        if (!data) return null;
        if (!user) return null;
        var token = user ? user.user_acc_tokn : ""
        var result = null;
        var url = "";
        var name = "";
        UserImgs.map(i => {
            if (i.id == data.proc_id) {
                url = i.url;
                name = i.name;
            }
        })
        if (!url || !name) {
            console.log("Not found proc_id");
            return null;
        }
        const formData = new FormData()
        formData.append(name, data.photo);
        formData.append('proc_id', data.proc_id);
        formData.append('proc_img_indx', data.proc_img_indx);

        await fetch(BASE_URL + url, {
            method: "POST",
            body: formData,
            headers: getHeadersImage(token)
        })
            .then(response => response.json())
            .then(response => {
                result = response;
            })
            .catch(error => {
                console.log("upload error", error);
                // alert("Upload failed!");
            });
        return result;
    }
    // deleteRequestData: async (url, token) => {
    //     var result = await axios.delete(BASE_URL + url, {
    //         headers: getHeaders(token)
    //     })
    //         .then(response => {
    //             return response.data;
    //         })
    //         .catch(
    //             error => console.log(error)
    //         )
    //     return result;
    // },
    // postSendEmail: async function (url, params) {
    //     var result = await axios.post(url, {}, {
    //         headers: getHeaders(),
    //         params: params
    //     })
    //         .then(response => {
    //             return response.data;
    //         })
    //         .catch(
    //             error => console.log(error)
    //         )
    //     return result;
    // },
}

export default MyService;