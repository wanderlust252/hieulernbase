import * as types from './ActionTypes'


export const handleIncrease= (step) => {
    // console.log("check handleTest ",test)
    return {
        type: types.INCREASE,
        step:step
    }
}
export const handleDecrease= (step) => {
    // console.log("check handleTest ",test)
    return {
        type: types.DECREASE,
        step:step
    }
}
