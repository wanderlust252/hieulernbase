import * as types from '../action/ActionTypes'
const defaultState = {
    value: 0,

}
const ReducerTest = (time = 0, action) => {
    switch (action.type) {
        case types.INCREASE:
            return time+action.step;
        case types.DECREASE:
            return time-action.step
        default:
            return time;
    }

};
export default ReducerTest;