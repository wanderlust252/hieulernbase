import {DECREASE,INCREASE} from '../action/ActionTypes' 
import {delay} from 'redux-saga'
import {put, takeEvery} from 'redux-saga/effects'
export function* sayHello(){
    console.log("HELLO!")
}
function* increment(){
    console.log("this is increment saga")
}
export function* watchIncrement(){
    yield takeEvery(INCREASE,increment)
}
function* decrement(){
    console.log(" this is decrement saga")
}
export function* watchDecrement(){
    console.log("watch decrement")
    yield takeEvery(DECREASE,decrement)
}
