import { createStore ,applyMiddleware} from 'redux';
import reducer from '../reducer';
import createSagaMiddleWare from 'redux-saga'
import rootSaga from '../sagas/rootSaga'
const sagaMiddleWare = createSagaMiddleWare();

// const store = createStore(reducer);
const store = createStore(reducer,applyMiddleware(sagaMiddleWare));
sagaMiddleWare.run(rootSaga)
export default store;
