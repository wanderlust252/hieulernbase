import { createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import Home from './module/Home'
import SecondScreen from './module/SecondScreen'

const RootStack = createStackNavigator({
    Home:{
        screen: Home
    },
    SecondScreen:{
        screen:SecondScreen
    }
})
export default createAppContainer(RootStack)