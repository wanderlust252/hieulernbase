/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux';
import { handleDecrease,handleIncrease } from '../../core/Redux/action/ActionTest';
class Controller extends Component {
  constructor(props) {
    super(props)
    this.state = {
      btnColor:'#db4c42'
    }
  }
  changeColor() {
    this.setState({ btnColor: this.getRandomColor() })
  }
  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <View>
          <Text>
            {this.props.time}
          </Text>
        </View>
        <View style={{ flexDirection: 'row' }} >
          <TouchableOpacity style={{ padding: 10 }}
            onPress={() => {
              this.props.onIncrement()
            }}>
            <Text>
              Tăng
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{ padding: 10 }}
            onPress={() => {
              this.props.onDecrement()
            }}>
            <Text>
              Giảm
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={{
            width: 100, height: 100, backgroundColor: this.state.btnColor,
            justifyContent: 'center', alignItems: 'center'
          }}
          onPress={() => {
            this.changeColor()
          }}

        >
          <Text>Click me ^^</Text>
        </TouchableOpacity>
      </View>
    );
  }


}
function mapStateToProps(state) {
  return {
    time: state.test? state.test:0
  }
}
const mapDispatchToProps = (dispatch)=>{
  return {
    onDecrement:(step)=>{
      dispatch(handleDecrease(step))
    },
    onIncrement:(step)=>{
      dispatch(handleIncrease(step))
    }
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Controller);

const styles = StyleSheet.create({

}); 
