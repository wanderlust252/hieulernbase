/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux';
import { handleDecrease,handleIncrease } from '../../core/Redux/action/ActionTest';
import  UserAPI  from "../../core/API/User/UserAPI";
class Controller extends Component {
  constructor(props) {
    super(props)
    this.state={ 
    }
  } 
  callGetAllUser = async ()=>{
    return await UserAPI.getAllUser();
  }
  componentDidMount(){
    this.callGetAllUser().then((val)=>{
      console.log("val = ",val)
    })
    
  }
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',flexDirection:'column' }}
      >
        <View>
          <Text>
            {this.props.time}
          </Text>
        </View>
        <View style={{flexDirection:'row'}} >
          <TouchableOpacity style={{padding:10}} 
          onPress={()=>{
            this.props.onIncrement(1)
          }}>
            <Text>
              Tăng
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{padding:10}}
          onPress={()=>{
            this.props.onDecrement(1)
          }}>
            <Text>
              Giảm
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={{
            width: 100, height: 100, backgroundColor: (this.state.btnColor)?'#db4c42':'#3577db',
            justifyContent: 'center', alignItems: 'center'
          }}
          onPress={()=>{
            // this.changeColor()
            this.props.navigation.navigate("SecondScreen")
          }}

        >
          <Text>Next Screen</Text>
        </TouchableOpacity>
      </View>
    );
  }


}
function mapStateToProps(state) {
  return {
    time: state.test? state.test:0
  }
}
const mapDispatchToProps = (dispatch)=>{
  return {
    onDecrement:(step)=>{
      dispatch(handleDecrease(step))
    },
    onIncrement:(step)=>{
      dispatch(handleIncrease(step))
    }
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Controller);

const styles = StyleSheet.create({

}); 

