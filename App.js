/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import { NavigationActions } from 'react-navigation'
import Router from './src/Router'
import { Provider } from 'react-redux'
import store from './src/core/Redux/store'
class App extends Component {
  constructor(props) {
    super(props)
    this.state={
      btnColor: true
    }
  }
  changeColor(){
    this.setState({btnColor:!this.state.btnColor})
  }
  render() {
    return (
      <Provider store={store}>
      <Router />
    </Provider>
    );
  }


}

const styles = StyleSheet.create({

});

export default App;
